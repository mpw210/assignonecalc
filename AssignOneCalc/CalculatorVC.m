//
//  ViewController.m
//  Assignment 1 : Calculator
//
//  CS 4768
//
//  Created by Mark Windsor 201235017 on 9/20/18.
//  Copyright © 2018 FLO. All rights reserved.
//

#import "CalculatorVC.h"
#import "math.h"

@interface CalculatorVC ()


@end




@implementation CalculatorVC {

    NSString *operand;
    NSString *resultString;
    double   firstValue;
    double   secondValue;
    BOOL     firstValueEntered;
    BOOL     twoValueOpSelected;
    UIImage  *selectedImage;
}

@synthesize resultLabel, typeOfCalculation;

- (void)viewDidLoad {
    [super viewDidLoad];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Calculation Methods

-(void)calculateResult {
    
    if ([self->operand isEqualToString:@"ADD"]){
        double resultValue = self->firstValue + self->secondValue;
        self->resultLabel.text = [[NSNumber numberWithDouble:resultValue] stringValue];
        NSLog(@"%@", [[NSNumber numberWithDouble:resultValue] stringValue]);
    
    } else if([self->operand isEqualToString:@"SUBTRACT"]){
        double resultValue = self->firstValue - self->secondValue;
        self->resultLabel.text = [[NSNumber numberWithDouble:resultValue] stringValue];
        NSLog(@"%@", [[NSNumber numberWithDouble:resultValue] stringValue]);
    
    } else if([self->operand isEqualToString:@"MULTIPLY"]){
        double resultValue = self->firstValue * self->secondValue;
        self->resultLabel.text = [[NSNumber numberWithDouble:resultValue] stringValue];
        NSLog(@"%@", [[NSNumber numberWithDouble:resultValue] stringValue]);
        
    } else if([self->operand isEqualToString:@"DIVIDE"]){
        double resultValue = self->firstValue / self->secondValue;
        self->resultLabel.text = [[NSNumber numberWithDouble:resultValue] stringValue];
        NSLog(@"%@", [[NSNumber numberWithDouble:resultValue] stringValue]);
    
    } else if([self->operand isEqualToString:@"NEGATE"]){
        double resultValue = -(self->firstValue);
        self->resultLabel.text = [[NSNumber numberWithDouble:resultValue] stringValue];
        NSLog(@"%@", self->resultLabel.text);
   
    
    } else if([self->operand isEqualToString:@"INVERSE"]){
        double resultValue = 1 / self->firstValue;
        self->resultLabel.text = [[NSNumber numberWithDouble:resultValue] stringValue];
        NSLog(@"%@", [[NSNumber numberWithDouble:resultValue] stringValue]);
    
    } else if([self->operand isEqualToString:@"ROOT"]){
        double resultValue = sqrt(self->firstValue);
        self->resultLabel.text = [[NSNumber numberWithDouble:resultValue] stringValue];
        NSLog(@"%@", [[NSNumber numberWithDouble:resultValue] stringValue]);
    
    } else if([self->operand isEqualToString:@"SQUARE"]){
        double resultValue = pow(self->firstValue, 2);
        self->resultLabel.text = [[NSNumber numberWithDouble:resultValue] stringValue];
        NSLog(@"%@", [[NSNumber numberWithDouble:resultValue] stringValue]);
    
    } else if([self->operand isEqualToString:@"CUBE"]){
        double resultValue = pow(self->firstValue, 3);
        self->resultLabel.text = [[NSNumber numberWithDouble:resultValue] stringValue];
        NSLog(@"%@", [[NSNumber numberWithDouble:resultValue] stringValue]);
    
    } else if([self->operand isEqualToString:@"POLY"]){
        double resultValue = pow(self->firstValue, self->secondValue);
        self->resultLabel.text = [[NSNumber numberWithDouble:resultValue] stringValue];
        NSLog(@"%@", [[NSNumber numberWithDouble:resultValue] stringValue]);
    
    } else {
        NSLog(@"Calculation Error");
    }
    
    
}

//Helper Method that returns a boolean if result label already has a decimial
- (BOOL)doesResultContainDecimal:(NSString*) string {
    NSString *decimal = @".";
    NSRange range = [self->resultLabel.text rangeOfString:decimal];
    if (range.location != NSNotFound) {
        return YES;
    }
    return NO;
}


    
    
#pragma mark - Button Actions

// Action for entering number.. only do we set firstValue entered to true when we tap an operand that needs two values to calculate
- (IBAction)numberTapped:(UIButton *)sender {

    if(!firstValueEntered){
        NSString *numberString       = sender.titleLabel.text;
        NSString *currentLabelString = resultLabel.text;
        self->resultLabel.text       = [currentLabelString isEqualToString:@"0"] ? numberString : [currentLabelString stringByAppendingFormat:@"%@", numberString];
        self->firstValue             = [self->resultLabel.text doubleValue];
        
    } else {
        NSString *numberString       = sender.titleLabel.text;
        NSString *currentLabelString = self->resultLabel.text;
        self->resultLabel.text       = [currentLabelString isEqualToString:@"0"] ? numberString : [currentLabelString stringByAppendingFormat:@"%@", numberString];
        self->secondValue            = [self->resultLabel.text doubleValue];
    }
}




//Action for appending decimal to the end of the result label
- (IBAction)decimalTapped:(id)sender {
    if ([self doesResultContainDecimal:self->resultLabel.text] == NO){
        self->resultLabel.text = [self->resultLabel.text stringByAppendingFormat:@"."];
    }
    
}


//Clear the first/second tapped value, the result label and the operand
- (IBAction)clearTapped:(id)sender {
    
    self->firstValueEntered      = false;
    self->firstValue             = 0;
    self->secondValue            = 0;
    self->resultLabel.text       = @"0";
    self->operand                = @"";
    self->typeOfCalculation.text = @"";
    
}

//remove last entered number from result label
- (IBAction)deleteTapped:(id)sender {
    if(!self->firstValueEntered){
        self->resultLabel.text = [self->resultLabel.text substringToIndex:[self->resultLabel.text length] - 1];
        self->firstValue       = [self->resultLabel.text doubleValue];
    } else {
        self->resultLabel.text = [self->resultLabel.text substringToIndex:[self->resultLabel.text length] - 1];
        self->secondValue      = [self->resultLabel.text doubleValue];
    }
}

//Equals tapped only for calculations that require two values
- (IBAction)equalsTapped:(id)sender {
  
    if([self->operand isEqualToString:@""]){
        return;
    } else {
        [self calculateResult];
    }
    
}


//Tags set in storyboard are used to identify which operand is tapped
- (IBAction)operandTapped:(UIButton *)sender {
    
    //With the set of operands we have, there are operands that need two values
    //and operands that need one value. For the ones that required one value..
    //..we will call the calcute method in this action. For the ones that require
    //2 values, we will set the button to selected and wait for the 2nd value.
    //Only when equals is tapped are these equations calculated
    
    switch(sender.tag){
        case 1:
            self->operand                = @"ADD";
            self->typeOfCalculation.text = @"+";
            self->resultLabel.text       = @"0";
            firstValueEntered            = true;
            break;
        case 2:
            self->operand                = @"SUBTRACT";
            self->typeOfCalculation.text = @"-";
            self->resultLabel.text       = @"0";
            firstValueEntered            = true;
            break;
        case 3:
            self->operand                = @"MULTIPLY";
            self->typeOfCalculation.text = @"*";
            self->resultLabel.text       = @"0";
            firstValueEntered            = true;
            break;
        case 4:
            self->operand                = @"DIVIDE";
            self->typeOfCalculation.text = @"/";
            self->resultLabel.text       = @"0";
            firstValueEntered            = true;
            break;
        case 5:
            self->operand                = @"NEGATE";
            self->typeOfCalculation.text = [NSString stringWithFormat:@"Negate of %f:", self->firstValue];
            [self calculateResult];
            break;
        case 6:
            self->operand                = @"INVERSE";
            self->typeOfCalculation.text = [NSString stringWithFormat:@"Inverse of %f:", self->firstValue];
            [self calculateResult];
            break;
        case 7:
            self->operand                = @"ROOT";
            self->typeOfCalculation.text = [NSString stringWithFormat:@"Root of %f:", self->firstValue];
            [self calculateResult];
            break;
        case 8:
            self->operand                = @"SQUARE";
            self->typeOfCalculation.text = [NSString stringWithFormat:@"Square of %f:", self->firstValue];
            [self calculateResult];
            break;
        case 9:
            self->operand                = @"CUBE";
            self->typeOfCalculation.text = [NSString stringWithFormat:@"Cube of %f:", self->firstValue];
            [self calculateResult];
            break;
        case 10:
            self->operand                = @"POLY";
            self->resultLabel.text       = @"0";
            self->typeOfCalculation.text = @"x^y:";
            firstValueEntered = YES;
            break;
        default : {
            NSLog(@"Tapping operand problem");
        }
            NSLog(@"%@", self->operand);
    }
}
    


    
    
    
    
    
    
    
    
    
    
@end
