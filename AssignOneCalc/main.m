//
//  main.m
//  AssignOneCalc
//
//  Created by Mark Windsor on 9/20/18.
//  Copyright © 2018 FLO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
