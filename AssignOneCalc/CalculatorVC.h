//
//  ViewController.h
//  AssignOneCalc
//
//  Created by Mark Windsor on 9/20/18.
//  Copyright © 2018 FLO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalculatorVC : UIViewController

//Label Showing Result of Calculation
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;

//Label showing type of calculation
@property (weak, nonatomic) IBOutlet UILabel *typeOfCalculation;

//Action for tapping numbers 0-9
- (IBAction)numberTapped:(UIButton *)sender;

//Action for decimal tap
- (IBAction)decimalTapped:(id)sender;

//Action to clear result label
- (IBAction)clearTapped:(id)sender;

//Action to delete tap
- (IBAction)deleteTapped:(id)sender;

//Action for calculation and displaying result
- (IBAction)equalsTapped:(id)sender;
    
//Action for the operand buttons
- (IBAction)operandTapped:(UIButton *)sender;
    
    

@end

