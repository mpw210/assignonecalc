//
//  AppDelegate.h
//  AssignOneCalc
//
//  Created by Mark Windsor on 9/20/18.
//  Copyright © 2018 FLO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

